# CHBase DataTypes tranform using FHIR Mapping language

1. Allergy

```
java -jar org.hl7.fhir.validator.jar ./source/allergy-fhir-source.xml -transform http://chbase.com/StructureMap/ToCHBaseAllergy -version 4.0.1 -ig ./logical -ig ./map -log log.txt -output ./output/allergy-chbase-output.xml
```
